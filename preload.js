// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
window.addEventListener("DOMContentLoaded", () => {
  const replaceText = (selector, text) => {
    const element = document.getElementById(selector);
    if (element) element.innerText = text;
  };

  for (const type of ["chrome", "node", "electron"]) {
    replaceText(`${type}-version`, process.versions[type]);
  }
});

const { ipcRenderer } = require("electron");
const { contextBridge } = require("electron");

contextBridge.exposeInMainWorld("api", {
  setup(options = {}) {
    ipcRenderer.on("onAlert", (event, messages) => {
      options.onAlert && options.onAlert();
    });
    ipcRenderer.send("setup");
  },
  getChecks: () => {
    return ipcRenderer.invoke("getChecks");
  },
  getStatus: () => {
    return ipcRenderer.invoke("getStatus");
  },
  setMode: (mode) => {
    return ipcRenderer.invoke("setMode", mode);
  },
});
