if (require("electron-squirrel-startup")) return;

let appExpired = false;
let diff = require("moment")().diff(require("moment")("2021-12-15"), "days");
if (diff >= 0) {
  appExpired = true;
}

const path = require("path");

//require("electron-reload")(__dirname, {
//electron: path.join(__dirname, "node_modules", ".bin", "electron-rebuild"),
//hardResetMethod: "exit",
//});

// Modules to control application life and create native browser window
const { app, BrowserWindow } = require("electron");

const { ipcMain } = require("electron");
const moment = require("moment");

let checks = 0;
let checkInfos = [];
let modes = {
  depot_dossier: "https://www.herault.gouv.fr/booking/create/15253",
  depot_dossier_urgent: "https://www.herault.gouv.fr/booking/create/35003",
  questions: "https://www.herault.gouv.fr/booking/create/34949",
  recepisse_update: "https://www.herault.gouv.fr/booking/create/36811",
};
let mode = "";
let checkTimeout = null;
let browserEvent = null;

ipcMain.handle("getChecks", (event, ...args) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(checks), 1000);
  });
});
ipcMain.handle("getStatus", (event, ...args) => {
  return new Promise((resolve, reject) => {
    setTimeout(
      () =>
        resolve({
          message: appExpired
            ? `The app has expired to avoid malicious use.`
            : `Days until app expire: ${Math.abs(diff)} `,
          code: appExpired ? "EXPIRED" : "OPERATIONAL",
        }),
      1000
    );
  });
});
ipcMain.handle("setMode", async (event, _mode) => {
  mode = _mode;
  console.log("setMode", mode);
  if (checkTimeout) {
    clearTimeout(checkTimeout);
  }
  checkRdv().catch(onError);
  return true;
});
ipcMain.on("setup", (event, args) => {
  browserEvent = event;
});

let page = null;
const puppeteer = require("puppeteer");
const sander = require("sander");
const getLocalPath = (p) => require("path").join(process.cwd(), "data.o", p);
const saveInfos = async (fail, checksCount) => {
  checkInfos.push({
    date: moment().format("DD-MM-YYYY HH:mm:ss"),
    fail,
    checks: checksCount,
  });
  await sander.writeFile(
    getLocalPath("infos.json"),
    JSON.stringify(checkInfos, null, 4)
  );
};

function onError(err) {
  console.log("ERROR", err);
  checkTimeout = setTimeout(() => {
    checkRdv().catch(onError);
  }, 1000 * 60 * 2);
}

checkRdv().catch(onError);

async function checkRdv() {
  if (!mode || appExpired) {
    return;
  }

  if (!page) {
    const browser = await puppeteer.launch({
      headless: false,
    });
    page = await browser.newPage();
  }
  await page.goto(modes[mode] || modes.depot_dossier);
  await page.$eval("body", (e) => {
    let input = e.querySelector('input[type="checkbox"');
    input.checked = true;
    return input ? true : false;
  });

  await Promise.all([
    page.waitForNavigation({
      timeout: 1000 * 5,
    }),
    page.$eval('input[type="submit"', (e) => {
      e && e.click();
      return e ? true : false;
    }),
  ]);

  //Sometimes, there is a radio button choice
  try {
    const extraStep = page.$eval('#fchoix_Booking', (e) => {
      return e.innerHTML.includes('souhaitez-vous')
    })
    if (extraStep) {
      await page.$eval("body", (e) => {
        let input = e.querySelector('input[type="radio"');
        input.checked = true;
        return input ? true : false;
      });
      await Promise.all([
        page.waitForNavigation({
          timeout: 1000 * 5,
        }),
        page.$eval('input[type="submit"', (e) => {
          e && e.click();
          return e ? true : false;
        }),
      ]);
    }
  } catch (err) {
    //Ignore if not found
  }


  let fail = await page.$eval('input[type="submit"', (e) => {
    try {
      let parent = e.parentElement.parentElement.parentElement.parentElement;
      if (parent && parent.innerHTML.indexOf("Il n'") !== -1) {
        return true;
      } else {
        return `INNER: ` + parent.innerHTML;
      }
    } catch (err) {
      return err.message;
    }
  });
  checks++;

  await saveInfos(fail, checks);

  if (fail === true) {
    checkTimeout = setTimeout(() => {
      checkRdv().catch(onError);
    }, 1000 * 60 * 2);
  } else {
    browserEvent && browserEvent.sender.send("onAlert");
    console.log(fail);
    checkTimeout = setTimeout(() => {
      checkRdv().catch(onError);
    }, 1000 * 60 * 60);
  }
}

function createWindow() {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 450,
    height: 500,
    webPreferences: {
      preload: path.join(__dirname, "preload.js"),
    },
  });

  mainWindow.removeMenu();

  // and load the index.html of the app.
  mainWindow.loadFile("index.html");
  if (process.env.NODE_ENV === "development") {
    mainWindow.webContents.openDevTools();
  }

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow();

  app.on("activate", function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", function () {
  if (process.platform !== "darwin") app.quit();
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
