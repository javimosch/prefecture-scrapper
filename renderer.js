// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.

new Vue({
  template: `
    <div>
        <div class="status" v-show="status">
          App status: {{status.message}}
        </div>
        
      <div v-show="!status || status.code!=='EXPIRED'">
        <label>Checks counter</label>
        <span>{{checks}}</span>
        <div>
          <label>Type of RDV to check</label>
          <select v-model="mode">
            <option value="depot_dossier">Dépôt d'un dossier à Montpellier </option>
            <option value="depot_dossier_urgent">Dépôt d'un dossier à Montpellier (demande urgente - titre périmé)</option>
            <option value="recepisse_update">RENOUVELLEMENT DE RÉCÉPISSÉS à Montpellier</option>
            <option value="questions">Renseignement démarche étranger à Montpellier</option>
          </select>
          <button class="stop_alert_sound_button" v-show="playingAlertSound" @click="stopSound">Stop sound</button>
        </div>
        <p>A browser will automatically check each 2 minutes. An alert sound will play if a date slot seems available. You will need to continue the booking process in that browser as fast as you can.</p>
        <p>Please report any errors or false positives (with an attached screenshot if possible) to arancibiajav@gmail.com</p>
        <p>
        This tool is free and created by misitioba.com
        </p>
      </div>
    </div>
      `,
  data() {
    return {
      status: "",
      checks: 0,
      mode: "",
      playingAlertSound: false,
    };
  },
  created() {
    console.log("created");
    this.$mount("#app");
  },
  watch: {
    mode() {
      window.api.setMode(this.mode);
      console.log("setMode", this.mode);
    },
  },
  mounted() {
    const self = this;
    async function updateChecks() {
      self.checks = await window.api.getChecks();
      self.status = await window.api.getStatus();
      setTimeout(() => {
        updateChecks();
      }, 1000);
    }
    updateChecks();

    window.api.setup({
      onAlert() {
        self.stopSound();
        self.playingAlertSound = true;
        self.sound = new Howl({
          src: ["alert.mp3"],
          autoplay: true,
        });
      },
    });
  },
  methods: {
    stopSound() {
      this.sound && this.sound.stop();
      this.playingAlertSound = false;
    },
  },
});
